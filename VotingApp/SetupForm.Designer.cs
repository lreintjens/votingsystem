﻿namespace VotingApp
{
    partial class SetupForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.startVote = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.optionsPanel = new System.Windows.Forms.Panel();
            this.deleteOption = new System.Windows.Forms.Button();
            this.addOption = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.statement = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // startVote
            // 
            this.startVote.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.startVote.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startVote.Location = new System.Drawing.Point(413, 541);
            this.startVote.Name = "startVote";
            this.startVote.Size = new System.Drawing.Size(225, 36);
            this.startVote.TabIndex = 0;
            this.startVote.Text = "Begin Stemming";
            this.startVote.UseVisualStyleBackColor = true;
            this.startVote.Click += new System.EventHandler(this.startVote_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(25, 90);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 28);
            this.label1.TabIndex = 1;
            this.label1.Text = "Kies Opties";
            // 
            // optionsPanel
            // 
            this.optionsPanel.Location = new System.Drawing.Point(25, 130);
            this.optionsPanel.Name = "optionsPanel";
            this.optionsPanel.Size = new System.Drawing.Size(544, 300);
            this.optionsPanel.TabIndex = 2;
            // 
            // deleteOption
            // 
            this.deleteOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteOption.Location = new System.Drawing.Point(160, 436);
            this.deleteOption.Name = "deleteOption";
            this.deleteOption.Size = new System.Drawing.Size(129, 29);
            this.deleteOption.TabIndex = 3;
            this.deleteOption.Text = "Verwijder Optie";
            this.deleteOption.UseVisualStyleBackColor = true;
            this.deleteOption.Click += new System.EventHandler(this.deleteOption_Click);
            // 
            // addOption
            // 
            this.addOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addOption.Location = new System.Drawing.Point(25, 436);
            this.addOption.Name = "addOption";
            this.addOption.Size = new System.Drawing.Size(129, 29);
            this.addOption.TabIndex = 2;
            this.addOption.Text = "Extra Optie";
            this.addOption.UseVisualStyleBackColor = true;
            this.addOption.Click += new System.EventHandler(this.addOption_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(20, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(198, 28);
            this.label2.TabIndex = 3;
            this.label2.Text = "Stelling/Onderwerp";
            // 
            // statement
            // 
            this.statement.Location = new System.Drawing.Point(25, 53);
            this.statement.Name = "statement";
            this.statement.Size = new System.Drawing.Size(521, 22);
            this.statement.TabIndex = 4;
            // 
            // SetupForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(650, 589);
            this.Controls.Add(this.deleteOption);
            this.Controls.Add(this.statement);
            this.Controls.Add(this.addOption);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.optionsPanel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.startVote);
            this.Name = "SetupForm";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button startVote;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel optionsPanel;
        private System.Windows.Forms.Button deleteOption;
        private System.Windows.Forms.Button addOption;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox statement;
    }
}

