﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VotingApp
{
    public partial class VotingForm : Form
    {
        Schulze schulze;

        public VotingForm(Schulze schulze)
        {
            this.schulze = schulze;
            InitializeComponent();

            label1.Text = schulze.statement;
            
            for(int i=0; i < schulze.options.Count; i++)
            {
                var numbox = new NumericUpDown();
                numbox.Name = schulze.options[i] + "numbox";
                var optionbox = new Label();
                optionbox.Text = schulze.options[i];
                numbox.Location = new Point(0, 40 * i);
                optionbox.Location = new Point(50, 40 * i);
                numbox.Size = new Size(35, 23);
                optionbox.Size = new Size(500, 23);
                optionsPanel.Controls.Add(numbox);
                optionsPanel.Controls.Add(optionbox);
            }
        }

        private void addVote_Click(object sender, EventArgs e)
        {
            Vote vote = new Vote();

            foreach (string option in schulze.options)
                vote.preferences.Add(option, (int) ((NumericUpDown) optionsPanel.Controls.Find(option + "numbox", false).Single()).Value);

            schulze.AddVote(vote);

            this.Close();
        }
    }
}
