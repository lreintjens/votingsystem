﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VotingApp
{
    public partial class SetupForm : Form
    {
        int options = 0;

        public SetupForm()
        {
            InitializeComponent();
            addOption_Click(null, null);
            addOption_Click(null, null);
        }

        private void startVote_Click(object sender, EventArgs e)
        {
            string stat = statement.Text;
            var opts = new List<string>();
            for (int i = 0; i < options; i++)
            {
                if (opts.Contains(optionsPanel.Controls[i].Text))
                {
                    MessageBox.Show("Alle opties moeten uniek zijn.");
                    return;
                }
                opts.Add(optionsPanel.Controls[i].Text);
            }

            var schulze = new Schulze(stat, opts);
            var votingForm = new OverviewForm(schulze);
            votingForm.Show();
            this.Hide();
        }

        private void addOption_Click(object sender, EventArgs e)
        {
            var optionbox = new TextBox();
            optionsPanel.Controls.Add(optionbox);
            optionbox.Location = new Point(0, 40 * options);
            optionbox.Size = new Size(400, 22);
            options++;
        }

        private void deleteOption_Click(object sender, EventArgs e)
        {
            if (options > 0)
            {
                optionsPanel.Controls.RemoveAt(optionsPanel.Controls.Count - 1);
                options--;
            }
        }
    }
}
