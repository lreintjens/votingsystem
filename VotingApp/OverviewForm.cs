﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VotingApp
{
    public partial class OverviewForm : Form
    {
        Schulze schulze;
        public OverviewForm(Schulze schulze)
        {
            this.schulze = schulze;
            InitializeComponent();

            var text = schulze.statement + "\n";
            foreach (string opt in schulze.options)
                text += "\n" + opt; 
            label1.Text = text;
        }

        private void Form2_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void addVote_Click(object sender, EventArgs e)
        {
            var votingForm = new VotingForm(schulze);
            var result = votingForm.ShowDialog();
        }

        private void endVoting_Click(object sender, EventArgs e)
        {
            var winners = schulze.CalculateVote();
            var text = "Winnende optie(s):";
            foreach (var w in winners)
                text += "\n" + w;
            MessageBox.Show(text);
        }
    }
}
