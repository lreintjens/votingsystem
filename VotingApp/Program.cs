﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VotingApp
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new SetupForm());
        }
    }

    public class Schulze
    {
        public string statement;
        public readonly List<string> options;
        List<Vote> votes;


        public Schulze(string statement, List<string> options)
        {
            this.statement = statement;
            this.options = options;
            votes = new List<Vote>();
        }

        public void AddVote(Vote vote)
        {
            votes.Add(vote);
        }

        public List<string> CalculateVote()
        {
            Console.WriteLine("Calculate votes");
            Console.WriteLine("Matrix pairwise preferences");
            Console.Write("    ");
            for (int i = 0; i < options.Count; i++)
                Console.Write("D*" + i + " ");
            Console.WriteLine();
            for (int i = 0; i < options.Count; i++)
            {
                Console.Write("D" + i + "*");
                for (int j = 0; j < options.Count; j++)
                    if (i == j)
                        Console.Write("    ");
                    else
                    {
                        string os = Preference(options[i], options[j]).ToString();
                        while (os.Length < 4)
                            os = " " + os;
                        Console.Write(os);
                    }
                Console.WriteLine();
            }
            Console.WriteLine("--------");
            Dictionary<long, int> pathstrengths = new Dictionary<long, int>();

            for (int i = 0; i < options.Count; i++)
                for (int j = 0; j < options.Count; j++)
                    if (i != j)
                    {
                        int dij = Preference(options[i], options[j]);
                        int dji = Preference(options[j], options[i]);
                        if (dij > dji)
                        {
                            pathstrengths[(((long) i << 32) + j)] = dij;
                        }
                        else
                        {
                            pathstrengths[(((long)i << 32) + j)] = 0;
                        }
                    }

            for (int i = 0; i < options.Count; i++)
                for (int j = 0; j < options.Count; j++)
                    if (i != j)
                        for (int k=0; k<options.Count; k++)
                            if (i != k & j != k)
                            {
                                int pjk = pathstrengths[(((long)j << 32) + k)];
                                int pji = pathstrengths[(((long)j << 32) + i)];
                                int pik = pathstrengths[(((long)i << 32) + k)];
                                pathstrengths[(((long)j << 32) + k)] = Math.Max(pjk, Math.Min(pji, pik));
                            }

            Console.WriteLine("Matrix strongest path strength");
            Console.Write("    ");
            for (int i = 0; i < options.Count; i++)
                Console.Write("D*" + i + " ");
            Console.WriteLine();
            for (int i = 0; i < options.Count; i++)
            {
                Console.Write("D" + i + "*");
                for (int j = 0; j < options.Count; j++)
                    if (i == j)
                        Console.Write("    ");
                    else
                    {
                        string os = pathstrengths[(((long)i << 32) + j)].ToString();
                        while (os.Length < 4)
                            os = " " + os;
                        Console.Write(os);
                    }
                Console.WriteLine();
            }
            Console.WriteLine("--------");

            bool[] winner = new bool[options.Count];
            for (int i = 0; i < options.Count; i++)
            {
                winner[i] = true;
                for (int j = 0; j < options.Count; j++)
                {
                    if (i != j)
                    {
                        if (pathstrengths[(((long)j << 32) + i)] > pathstrengths[(((long)i << 32) + j)])
                        {
                            winner[i] = false;
                        }
                    }
                }
            }

            var result = new List<string>();
            for (int i = 0; i < options.Count; i++)
                if (winner[i])
                    result.Add(options[i]);
            return result;
        }

        int Preference(string A, string B)
        {
            int AOverB = 0;
            foreach (Vote v in votes){
                if (v.PrefersOver(A, B))
                    AOverB++;
            }
            return AOverB;
        }
    }

    public class Vote
    {
        public Dictionary<string, int> preferences;

        public Vote()
        {
            preferences = new Dictionary<string, int>();
        }

        public Vote(Dictionary<string, int> preferences)
        {
            this.preferences = preferences;
        }

        public bool PrefersOver(string A, string B)
        {
            return preferences[A] > preferences[B];
        }
    }
}
